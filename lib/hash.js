import { createHash } from 'node:crypto';

export default (
    data,
    {
        algorithm = 'sha256',
        encoding = 'hex'
    } = {}
) => createHash(algorithm).update(data).digest(encoding);