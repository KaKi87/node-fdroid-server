import fs from 'node:fs';
import childProcess from 'node:child_process';

import JSZip from 'jszip';

import {
    apksignerPath,
    keyStorePass
} from '../config.js';

export default async ({
    keyStorePath,
    path,
    files
}) => {
    const
        zip = new JSZip();

    for(const {
        name,
        content
    } of files) zip.file(
        name,
        content
    );

    await new Promise(resolve => zip
        .generateNodeStream({
            type: 'nodebuffer',
            streamFiles: true
        })
        .pipe(fs.createWriteStream(path))
        .on('finish', resolve)
    );

    await new Promise(resolve => childProcess.spawn(
        apksignerPath,
        [
            'sign',
            '--min-sdk-version=23',
            '--max-sdk-version=24',
            '--v1-signing-enabled=true',
            '--v2-signing-enabled=false',
            '--v3-signing-enabled=false',
            '--v4-signing-enabled=false',
            `--ks=${keyStorePath}`,
            '--ks-pass=env:KEY_STORE_PASS',
            path
        ],
        {
            env: {
                'KEY_STORE_PASS': keyStorePass
            }
        }
    ).on('exit', resolve));
};