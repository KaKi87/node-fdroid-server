import { join as joinPath } from 'node:path';
import fs from 'node:fs/promises';
import childProcess from 'node:child_process';

import APK from 'node-apk';
import JSZip from 'jszip';

import extractApk from './extractApk.js';
import getExtractedApkMetadata from './getExtractedApkMetadata.js';
import hash from './hash.js';

import {
    aaptPath
} from '../config.js';

const dataPath = joinPath(new URL('.', import.meta.url).pathname, '../data');

export default async path => {
    const
        file = await fs.readFile(path),
        apk = new APK.Apk(file),
        zip = new JSZip(),
        [
            manifestData,
            signatureData,,
            aaptData
        ] = await Promise.all([
            apk.getManifestInfo(),
            apk.getCertificateInfo(),
            zip.loadAsync(file),
            new Promise((resolve, reject) => childProcess.exec(
                `${aaptPath} dump badging ${path}`,
                (error, stdout, stderr) => {
                    if(error)
                        reject(error);
                    else if(stderr)
                        reject(stderr);
                    else
                        resolve(stdout);
                }
            ))
        ]),
        {
            'xml': {
                'attributes': {
                    versionCode,
                    versionName,
                    'package': id
                },
                'children': {
                    'uses-sdk': [
                        {
                            'attributes': {
                                minSdkVersion,
                                targetSdkVersion
                            }
                        }
                    ]
                }
            }
        } = manifestData,
        extractionPath = joinPath(dataPath, 'extractedApk', id, versionCode.toString());
    await extractApk(path, extractionPath);
    const {
        icon
    } = await getExtractedApkMetadata(extractionPath);
    return {
        id,
        name: aaptData
            .split('\n')
            .filter(line => line.startsWith('application-label-'))
            .reduce((locales, line) => ({
                ...locales,
                [line.slice(18).split(':')[0]]: line.split(`'`)[1]
            }), {}),
        versionCode,
        versionName,
        signerHexMd5Hash: hash(signatureData[0].bytes.toString('hex'), { algorithm: 'md5' }),
        signerSha256Hash: hash(signatureData[0].bytes),
        minSdkVersion,
        targetSdkVersion,
        permissions: manifestData['xml']['children']['uses-permission'].map(_ => _['attributes']['name']),
        nativeCodeArchitectures: [...new Set(
            Object
                .keys(zip.files)
                .filter(path => path.startsWith('lib/'))
                .map(path => path.split('/')[1])
        )],
        icon
    };
};