import childProcess from 'node:child_process';

import {
    gdownPath
} from '../config.js';

export default ({
    id,
    path
}) => new Promise(resolve => childProcess.spawn(
    gdownPath,
    [
        id,
        '-O',
        path
    ]
).on('exit', resolve));