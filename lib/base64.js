export const
    encode = string => Buffer
        .from(string)
        .toString('base64')
        .replace(/\+/g, '-')
        .replace(/\//g, '_')
        .replace(/=/, ''),
    decode = string => Buffer.from(string, 'base64').toString();