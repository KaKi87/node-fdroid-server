import axios from 'axios';
import linkify from 'linkifyjs';

export default async ({
    owner,
    repository
}) => {
    const {
        data
    } = await axios(
        `https://api.github.com/repos/${owner}/${repository}/releases`,
        {
            params: {
                'per_page': '100'
            }
        }
    );
    return data.map(item => ({
        tag: item['tag_name'],
        creationTimestamp: new Date(item['published_at']).valueOf(),
        assetsUrls: item['assets'].map(_ => _['browser_download_url']),
        bodyUrls: linkify.find(item['body']).map(_ => _.href)
    }));
};