import childProcess from 'node:child_process';

import {
    apktoolPath
} from '../config.js';

export default (
    inputPath,
    outputPath
) => new Promise(resolve => childProcess.spawn(
    apktoolPath,
    [
        'd',
        inputPath,
        '-o',
        outputPath
    ]
).on('exit', resolve));