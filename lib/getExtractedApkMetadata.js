import { readFile, readdir } from 'node:fs/promises';
import { join as joinPath } from 'node:path';
import { existsSync } from 'node:fs';

import { parseStringPromise } from 'xml2js';

export default async path => {
    const
        manifestData = (await parseStringPromise(await readFile(joinPath(path, 'AndroidManifest.xml'))))['manifest'],
        resourcesPath = joinPath(path, 'res'),
        iconFileName = `${manifestData['application'][0].$['android:icon'].split('/')[1]}.png`,
        iconFilePath = (await readdir(resourcesPath))
            .map(directoryName => joinPath(resourcesPath, directoryName, iconFileName))
            .filter(existsSync)
            .slice(-1)[0];
    return {
        icon: await readFile(iconFilePath)
    };
};