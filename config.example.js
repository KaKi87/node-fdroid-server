export const
    /**
     * `keytool` binary path
     * @type {string}
     */
    keytoolPath = undefined,
    /**
     * Key store password
     * @type {string}
     */
    keyStorePass = undefined,
    /**
     * Key password
     * @type {string}
     */
    keyPass = undefined,
    /**
     * Sequelize URI
     * @type {string}
     */
    sequelizeUri = 'sqlite::memory:',
    /**
     * Repository name
     * @type {string}
     */
    name = undefined,
    /**
     * Repository description
     * @type {string}
     */
    description = undefined,
    /**
     * Enable external files proxying
     * (required for official F-Droid client support)
     * @type {boolean}
     */
    isProxy = undefined,
    /**
     * Repository host
     * @type {string}
     */
    host = undefined,
    /**
     * Fastify host
     * @type {string}
     */
    fastifyHost = undefined,
    /**
     * Fastify port
     * @type {number}
     */
    fastifyPort = undefined,
    /**
     * `apksigner` executable path
     * @type {string}
     */
    apksignerPath = undefined,
    /**
     * `gdown` executable path
     * @type {string}
     */
    gdownPath = undefined,
    /**
     * `aapt` executable path
     * @type {string}
     */
    aaptPath = undefined,
    /**
     * `apktool` executable path
     * @type {string}
     */
    apktoolPath = undefined;