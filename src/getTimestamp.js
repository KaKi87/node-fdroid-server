export default async ({
    sequelize
}) => (await sequelize.models.Apk.findOne({
    attributes: ['updateTimestamp'],
    order: [['updateTimestamp', 'DESC']]
})).updateTimestamp.valueOf();