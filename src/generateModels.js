import { DataTypes } from 'sequelize';
import { nanoid } from 'nanoid';
import spdxLicenses from 'spdx-license-list/simple.js';
import SequelizeSlugify from 'sequelize-slugify';

const
    options = {
        timestamps: true,
        createdAt: 'creationTimestamp',
        updatedAt: 'updateTimestamp',
        freezeTableName: true
    },
    id = {
        type: DataTypes.STRING,
        defaultValue: () => nanoid(),
        primaryKey: true
    },
    name = {
        type: DataTypes.STRING,
        allowNull: false
    },
    url = () => ({
        type: DataTypes.STRING,
        validate: {
            isURL: [{
                protocols: ['https'],
                require_protocol: true,
                require_valid_protocol: true
            }]
        }
    }),
    sha256 = () => ({
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
        validate: {
            isHash: ['sha256']
        }
    });

export default sequelize => {
    const
        Author = sequelize.define(
            'Author',
            {
                id,
                email: {
                    type: DataTypes.STRING,
                    unique: true,
                    validate: {
                        isEmail: true
                    }
                },
                name,
                websiteUrl: {
                    ...url(),
                    unique: true
                }
            },
            options
        ),
        App = sequelize.define(
            'App',
            {
                id: {
                    type: DataTypes.STRING,
                    primaryKey: true,
                    validate: {
                        is: /^([A-Za-z][A-Za-z\d_]*\.)+[A-Za-z][A-Za-z\d_]*$/
                    }
                },
                authorId: {
                    type: DataTypes.STRING,
                    allowNull: false,
                    references: {
                        model: Author
                    }
                },
                changelogUrl: {
                    ...url(),
                    unique: true
                },
                license: {
                    type: DataTypes.STRING,
                    validate: {
                        isSpdx: value => {
                            if(!spdxLicenses.has(value))
                                throw new Error('License must be a valid SPDX');
                        }
                    }
                },
                repositoryUrl: {
                    ...url(),
                    unique: true
                },
                issueTrackerUrl: {
                    ...url(),
                    unique: true
                },
                translationUrl: {
                    ...url(),
                    unique: true
                },
                iconUrl: {
                    ...url(),
                    unique: true
                },
                iconSha256Hash: sha256(),
                iconSize: {
                    type: DataTypes.INTEGER,
                    allowNull: false
                }
            },
            options
        ),
        appId = {
            type: DataTypes.STRING,
            references: {
                model: App
            }
        };
    Author.hasMany(
        App,
        {
            as: 'apps',
            foreignKey: 'authorId'
        }
    );
    App.belongsTo(
        Author,
        {
            as: 'author',
            foreignKey: 'authorId'
        }
    );
    {
        const
            Locale = sequelize.define(
                'Locale',
                {
                    id: {
                        ...id,
                        primaryKey: false,
                        unique: true
                    },
                    appId: {
                        ...appId,
                        primaryKey: true
                    },
                    locale: {
                        type: DataTypes.STRING,
                        primaryKey: true,
                        validate: {
                            isLocale: true
                        }
                    },
                    name,
                    summary: DataTypes.STRING,
                    description: DataTypes.TEXT,
                    featureGraphicUrl: {
                        ...url(),
                        unique: true
                    },
                    featureGraphicSha256Hash: sha256(),
                    featureGraphicSize: {
                        type: DataTypes.INTEGER,
                        allowNull: false
                    },
                    tvBannerUrl: {
                        ...url(),
                        unique: true
                    }
                },
                options
            ),
            Screenshot = sequelize.define(
                'Screenshot',
                {
                    id,
                    localeId: {
                        type: DataTypes.STRING,
                        allowNull: false,
                        references: {
                            model: Locale
                        }
                    },
                    url: {
                        ...url(),
                        allowNull: false,
                        unique: true
                    },
                    sha256Hash: sha256(),
                    size: {
                        type: DataTypes.INTEGER,
                        allowNull: false
                    },
                    device: {
                        type: DataTypes.ENUM(
                            'phone',
                            'sevenInch',
                            'tenInch',
                            'tv',
                            'wear'
                        ),
                        allowNull: false
                    }
                },
                options
            );
        Locale.belongsTo(
            App,
            {
                as: 'app',
                foreignKey: 'appId'
            }
        );
        App.hasMany(
            Locale,
            {
                as: 'locales',
                foreignKey: 'appId'
            }
        );
        Screenshot.belongsTo(
            Locale,
            {
                as: 'locale',
                foreignKey: 'localeId'
            }
        );
        Locale.hasMany(
            Screenshot,
            {
                as: 'screenshots',
                sourceKey: 'id',
                foreignKey: 'localeId'
            }
        );
    }
    {
        const
            Apk = sequelize.define(
                'Apk',
                {
                    id: {
                        ...id,
                        primaryKey: false,
                        allowNull: false,
                        unique: true,
                    },
                    appId: {
                        ...appId,
                        primaryKey: true
                    },
                    versionCode: {
                        type: DataTypes.INTEGER,
                        primaryKey: true
                    },
                    versionName: {
                        type: DataTypes.STRING,
                        primaryKey: true
                    },
                    sha256Hash: sha256(),
                    signerHexMd5Hash: {
                        type: DataTypes.STRING,
                        allowNull: false,
                        unique: true,
                        validate: {
                            isHash: ['md5']
                        }
                    },
                    signerSha256Hash: sha256(),
                    minSdkVersion: {
                        type: DataTypes.INTEGER,
                        allowNull: false
                    },
                    targetSdkVersion: {
                        type: DataTypes.INTEGER,
                        allowNull: false
                    },
                    url: {
                        ...url(),
                        allowNull: false,
                        unique: true
                    },
                    size: {
                        type: DataTypes.INTEGER,
                        allowNull: false
                    }
                },
                options
            ),
            Permission = sequelize.define(
                'Permission',
                {
                    id: {
                        type: DataTypes.STRING,
                        primaryKey: true
                    }
                },
                options
            ),
            ApkPermission = sequelize.define(
                'ApkPermission',
                {
                    apkId: {
                        type: DataTypes.STRING,
                        primaryKey: true,
                        references: {
                            model: Apk
                        }
                    },
                    permissionId: {
                        type: DataTypes.STRING,
                        primaryKey: true,
                        references: {
                            model: Permission
                        }
                    }
                },
                options
            );
        Apk.belongsToMany(
            Permission,
            {
                as: 'permissions',
                through: ApkPermission,
                foreignKey: 'apkId',
                otherKey: 'permissionId',
                sourceKey: 'id',
                targetKey: 'id'
            }
        );
        Permission.belongsToMany(
            Apk,
            {
                as: 'apks',
                through: ApkPermission,
                foreignKey: 'permissionId',
                otherKey: 'apkId',
                sourceKey: 'id',
                targetKey: 'id'
            }
        );
        ApkPermission.belongsTo(
            Apk,
            {
                as: 'apk',
                foreignKey: 'apkId'
            }
        );
        Apk.hasMany(
            ApkPermission,
            {
                as: 'ApkPermissions',
                foreignKey: 'apkId'
            }
        );
        Apk.belongsTo(
            App,
            {
                as: 'app',
                foreignKey: 'appId'
            }
        );
        App.hasMany(
            Apk,
            {
                as: 'apks',
                foreignKey: 'appId'
            }
        );
        ApkPermission.belongsTo(
            Permission,
            {
                as: 'permission',
                foreignKey: 'permissionId'
            }
        );
        Permission.hasMany(
            ApkPermission,
            {
                as: 'ApkPermissions',
                foreignKey: 'permissionId'
            }
        );
    }
    {
        const
            Category = sequelize.define(
                'Category',
                {
                    slug: {
                        type: DataTypes.STRING,
                        primaryKey: true
                    },
                    name
                },
                options
            ),
            AppCategory = sequelize.define(
                'AppCategory',
                {
                    appId: {
                        ...appId,
                        primaryKey: true
                    },
                    categorySlug: {
                        type: DataTypes.STRING,
                        primaryKey: true,
                        references: {
                            model: Category
                        }
                    }
                },
                options
            );
        SequelizeSlugify.slugifyModel(
            Category,
            {
                source: ['name'],
                column: 'slug'
            }
        );
        App.belongsToMany(
            Category,
            {
                as: 'categories',
                through: AppCategory,
                foreignKey: 'appId',
                otherKey: 'categorySlug'
            }
        );
        Category.belongsToMany(
            App,
            {
                as: 'apps',
                through: AppCategory,
                foreignKey: 'categorySlug',
                otherKey: 'appId'
            }
        );
        AppCategory.belongsTo(
            App,
            {
                as: 'app',
                foreignKey: 'appId'
            }
        );
        App.hasMany(
            AppCategory,
            {
                as: 'AppCategories',
                foreignKey: 'appId'
            }
        );
        AppCategory.belongsTo(
            Category,
            {
                as: 'category',
                foreignKey: 'categorySlug'
            }
        );
        Category.hasMany(
            AppCategory,
            {
                as: 'AppCategories',
                foreignKey: 'categorySlug'
            }
        );
    }
};