import { join as joinPath } from 'node:path';
import fs from 'node:fs/promises';

import getApps from './getApps.js';
import { encode as encodeBase64 } from '../lib/base64.js';

import {
    name,
    description
} from '../config.js';

export default async ({
    dataPath,
    timestamp,
    sequelize,
    url
}) => {
    const filePath = joinPath(dataPath, `index-v2.${timestamp}.json`);
    let fileOrData;
    try {
        fileOrData = await fs.readFile(filePath);
    }
    catch {}
    if(!fileOrData){
        const apps = await getApps({ sequelize });
        fileOrData = {
            'repo': {
                'name': {
                    'en-US': name
                },
                'description': {
                    'en-US': description
                },
                'address': url,
                timestamp,
                'categories': [...new Set(apps.flatMap(app => app.categories.map(category => category.name)))]
                    .reduce((categories, category) => ({ ...categories, [category]: { 'name': { 'en-US': category } } }), {})
            },
            'packages': apps.reduce((
                apps,
                app
            ) => ({
                ...apps,
                [app.id]: {
                    'metadata': {
                        'added': app.creationTimestamp.valueOf(),
                        'categories': app.categories.map(category => category.name),
                        'changelog': app.changelogUrl,
                        'issueTracker': app.issueTrackerUrl,
                        'lastUpdated': app.updateTimestamp.valueOf(),
                        'license': app.license,
                        'sourceCode': app.repositoryUrl,
                        'translation': app.translationUrl,
                        'featureGraphic': app.locales.reduce((
                            locales,
                            locale
                        ) => ({
                            ...locales,
                            [locale.locale]: {
                                'name': `/file/${encodeBase64(JSON.stringify({ property: 'Locale.featureGraphicUrl', id: locale.id }))}`,
                                'sha256': locale.featureGraphicSha256Hash,
                                'size': locale.featureGraphicSize
                            }
                        }), {}),
                        'screenshots': {
                            'phone': app.locales.reduce((
                                locales,
                                locale
                            ) => ({
                                ...locales,
                                [locale.locale]: locale.screenshots
                                    .filter(screenshot => screenshot.device === 'phone')
                                    .map(screenshot => ({
                                        'name': `/file/${encodeBase64(JSON.stringify({ property: 'Screenshot.url', id: screenshot.id }))}`,
                                        'sha256': screenshot.sha256Hash,
                                        'size': screenshot.size
                                    }))
                            }), {}),
                            'sevenInch': app.locales.reduce((
                                locales,
                                locale
                            ) => ({
                                ...locales,
                                [locale.locale]: locale.screenshots
                                    .filter(screenshot => screenshot.device === 'sevenInch')
                                    .map(screenshot => ({
                                        'name': `/file/${encodeBase64(JSON.stringify({ property: 'Screenshot.url', id: screenshot.id }))}`,
                                        'sha256': screenshot.sha256Hash,
                                        'size': screenshot.size
                                    }))
                            }), {}),
                            'tenInch': app.locales.reduce((
                                locales,
                                locale
                            ) => ({
                                ...locales,
                                [locale.locale]: locale.screenshots
                                    .filter(screenshot => screenshot.device === 'tenInch')
                                    .map(screenshot => ({
                                        'name': `/file/${encodeBase64(JSON.stringify({ property: 'Screenshot.url', id: screenshot.id }))}`,
                                        'sha256': screenshot.sha256Hash,
                                        'size': screenshot.size
                                    }))
                            }), {}),
                            'tv': app.locales.reduce((
                                locales,
                                locale
                            ) => ({
                                ...locales,
                                [locale.locale]: locale.screenshots
                                    .filter(screenshot => screenshot.device === 'tv')
                                    .map(screenshot => ({
                                        'name': `/file/${encodeBase64(JSON.stringify({ property: 'Screenshot.url', id: screenshot.id }))}`,
                                        'sha256': screenshot.sha256Hash,
                                        'size': screenshot.size
                                    }))
                            }), {}),
                            'wear': app.locales.reduce((
                                locales,
                                locale
                            ) => ({
                                ...locales,
                                [locale.locale]: locale.screenshots
                                    .filter(screenshot => screenshot.device === 'wear')
                                    .map(screenshot => ({
                                        'name': `/file/${encodeBase64(JSON.stringify({ property: 'Screenshot.url', id: screenshot.id }))}`,
                                        'sha256': screenshot.sha256Hash,
                                        'size': screenshot.size
                                    }))
                            }), {})
                        },
                        'authorEmail': app.author.email,
                        'authorName': app.author.name,
                        'authorWebSite': app.author.websiteUrl,
                        'name': app.locales.reduce((
                            locales,
                            locale
                        ) => ({
                            ...locales,
                            [locale.locale]: locale.name
                        }), {}),
                        'summary': app.locales.reduce((
                            locales,
                            locale
                        ) => ({
                            ...locales,
                            [locale.locale]: locale.summary
                        }), {}),
                        'description': app.locales.reduce((
                            locales,
                            locale
                        ) => ({
                            ...locales,
                            [locale.locale]: locale.description
                        }), {}),
                        'icon': {
                            'en-US': {
                                'name': `/icons/${encodeBase64(JSON.stringify({ property: 'App.iconUrl', id: app.id }))}`,
                                'sha256': app.iconSha256Hash,
                                'size': app.iconSize
                            }
                        }
                    },
                    'versions': app.apks.reduce((
                        apks,
                        apk
                    ) => ({
                        ...apks,
                        [apk.sha256Hash]: {
                            'added': apk.creationTimestamp.valueOf(),
                            'file': {
                                'name': `${encodeBase64(JSON.stringify({
                                    property: 'Apk.url',
                                    id: apk.id,
                                    name: `${app.id}_${apk.versionCode}.apk`
                                }))}.apk`,
                                'sha256': apk.sha256Hash,
                                'size': apk.size
                            },
                            'manifest': {
                                'versionName': apk.versionName,
                                'versionCode': apk.versionCode,
                                'usesSdk': {
                                    'minSdkVersion': apk.minSdkVersion,
                                    'targetSdkVersion': apk.targetSdkVersion
                                },
                                'signer': {
                                    "sha256": [apk.signerSha256Hash]
                                },
                                'usesPermission': apk.permissions.map(permission => ({ 'name': permission.id }))
                            }
                        }
                    }), {})
                }
            }), {})
        };
        await fs.writeFile(filePath, JSON.stringify(fileOrData), 'utf8');
    }
    return fileOrData;
};