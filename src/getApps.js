export default ({
    sequelize
}) => sequelize.models.App.findAll({
    include: [
        {         model: sequelize.models.Author,     as: 'author',      required: true },
        {         model: sequelize.models.Category,   as: 'categories',  required: true },
        {
                  model: sequelize.models.Locale,     as: 'locales',     required: true  ,
            include: [
                { model: sequelize.models.Screenshot, as: 'screenshots'                 },
            ]
        },
        {
                  model: sequelize.models.Apk,        as: 'apks',        required: true  ,
            include: [
                { model: sequelize.models.Permission, as: 'permissions', required: true }
            ]
        }
    ]
});