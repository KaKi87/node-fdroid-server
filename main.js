import { join as joinPath } from 'node:path';
import fs from 'node:fs/promises';

import createFastify from 'fastify';
import { Sequelize } from 'sequelize';
import axios from 'axios';
import { encodeVersion } from 'js-byte-version-encoder';
import Joi from 'joi';

import getIndexV2 from './src/getIndexV2.js';
import getTimestamp from './src/getTimestamp.js';
import hash from './lib/hash.js';
import getApps from './src/getApps.js';
import generateSignedJar from './lib/generateSignedJar.js';
import packageJson from './package.json' assert { type: 'json' };
import { encode as encodeBase64, decode as decodeBase64 } from './lib/base64.js';
import generateModels from './src/generateModels.js';

import {
    host,
    sequelizeUri,
    name,
    description,
    isProxy,
    fastifyHost,
    fastifyPort
} from './config.js';

const
    fastify = createFastify({
        logger: true,
        maxParamLength: 110000
    }),
    dataPath = joinPath(new URL('.', import.meta.url).pathname, './data'),
    sequelize = new Sequelize(
        sequelizeUri,
        {
            logging: (message, data) => {
                fastify.log.debug(data);
                fastify.log.info(message);
            }
        }
    ),
    keyStorePath = joinPath(dataPath, './keystore'),
    url = `${host}/repo`;

axios.interceptors.request.use(config => {
    fastify.log.debug(config);
    fastify.log.info({
        method: config.method,
        url: config.url
    });
    return config;
});

axios.interceptors.response.use(
    response => {
        fastify.log.debug(response);
        fastify.log.info({
            method: response.request.method,
            url: response.request.url,
            status: response.status
        });
        return response;
    },
    error => {
        fastify.log.error(error);
        return Promise.reject(error);
    }
);

[
    '/repo/index-v2.json',
    '/fdroid/repo/index-v2.json'
].forEach(path => fastify.get(
    path,
    async (
        request,
        reply
    ) => {
        const indexV2 = await getIndexV2({
            dataPath,
            timestamp: await getTimestamp({ sequelize }),
            sequelize,
            url
        });
        if(Buffer.isBuffer(indexV2))
            reply.type('application/json');
        reply.send(indexV2);
    }
));

[
    '/repo/entry.jar',
    '/fdroid/repo/entry.jar'
].forEach(path => fastify.get(
    path,
    async (
        request,
        reply
    ) => {
        const
            timestamp = await getTimestamp({ sequelize }),
            filePath = joinPath(dataPath, `entry.${timestamp}.jar`);
        let file;
        try {
            file = await fs.readFile(filePath);
        }
        catch {}
        if(!file){
            let indexV2 = await getIndexV2({
                dataPath,
                timestamp,
                sequelize,
                url
            });
            if(!Buffer.isBuffer(indexV2))
                indexV2 = Buffer.from(JSON.stringify(indexV2));
            await generateSignedJar({
                keyStorePath,
                path: filePath,
                files: [
                    {
                        name: 'entry.json',
                        content: JSON.stringify({
                            timestamp,
                            'version': encodeVersion(packageJson.version),
                            'index': {
                                'name': '/index-v2.json',
                                'sha256': hash(indexV2),
                                'size': indexV2.length,
                                'numPackages': await sequelize.models.App.count()
                            },
                            'diffs': {}
                        })
                    }
                ]
            });
            file = await fs.readFile(filePath);
        }
        reply.send(file);
    }
));

[
    '/repo/index-v1.jar',
    '/fdroid/repo/index-v1.jar'
].forEach(path => fastify.get(
    path,
    async (
        request,
        reply
    ) => {
        const
            timestamp = await getTimestamp({ sequelize }),
            filePath = joinPath(dataPath, `index-v1.${timestamp}.jar`);
        let file;
        try {
            file = await fs.readFile(filePath);
        }
        catch {}
        if(!file){
            const apps = await getApps({ sequelize });
            await generateSignedJar({
                keyStorePath,
                path: filePath,
                files: [
                    {
                        name: 'index-v1.json',
                        content: JSON.stringify({
                            'repo': {
                                timestamp,
                                'version': encodeVersion(packageJson.version),
                                'name': name,
                                'address': url,
                                'description': description
                            },
                            'requests': {
                                'install': [],
                                'uninstall': []
                            },
                            'apps': apps.map(app => ({
                                'authorEmail': app.author.email,
                                'authorName': app.author.name,
                                'authorWebSite': app.author.websiteUrl,
                                'categories': app.categories.map(category => category.name),
                                'changelog': app.changelogUrl,
                                'license': app.license,
                                'sourceCode': app.repositoryUrl,
                                'added': app.creationTimestamp.valueOf(),
                                'icon': encodeBase64(JSON.stringify({ property: 'App.iconUrl', id: app.id })),
                                'packageName': app.id,
                                'lastUpdated': app.updateTimestamp.valueOf(),
                                'localized': app.locales.reduce((
                                    locales,
                                    locale
                                ) => ({
                                    ...locales,
                                    [locale.locale]: {
                                        'description': locale.description,
                                        'featureGraphic': encodeBase64(JSON.stringify({ property: 'Locale.featureGraphicUrl', id: locale.id })),
                                        'name': locale.name,
                                        'phoneScreenshots': locale.screenshots
                                            .filter(screenshot => screenshot.device === 'phone')
                                            .map(screenshot => encodeBase64(JSON.stringify({ property: 'Screenshot.url', id: screenshot.id }))),
                                        'sevenInchScreenshots': locale.screenshots
                                            .filter(screenshot => screenshot.device === 'sevenInch')
                                            .map(screenshot => encodeBase64(JSON.stringify({ property: 'Screenshot.url', id: screenshot.id }))),
                                        'tenInchScreenshots': locale.screenshots
                                            .filter(screenshot => screenshot.device === 'tenInch')
                                            .map(screenshot => encodeBase64(JSON.stringify({ property: 'Screenshot.url', id: screenshot.id }))),
                                        'tvScreenshots': locale.screenshots
                                            .filter(screenshot => screenshot.device === 'tv')
                                            .map(screenshot => encodeBase64(JSON.stringify({ property: 'Screenshot.url', id: screenshot.id }))),
                                        'wearScreenshots': locale.screenshots
                                            .filter(screenshot => screenshot.device === 'wear')
                                            .map(screenshot => encodeBase64(JSON.stringify({ property: 'Screenshot.url', id: screenshot.id }))),
                                        'summary': locale.summary
                                    }
                                }), {})
                            })),
                            'packages': apps.reduce((
                                apps,
                                app
                            ) => ({
                                ...apps,
                                [app.id]: app.apks.map(apk => ({
                                    'added': apk.creationTimestamp.valueOf(),
                                    'apkName': `${encodeBase64(JSON.stringify({
                                        property: 'Apk.url',
                                        id: apk.id,
                                        name: `${app.id}_${apk.versionCode}.apk`
                                    }))}.apk`,
                                    'hash': apk.sha256Hash,
                                    'hashType': 'sha256',
                                    'minSdkVersion': apk.minSdkVersion,
                                    'packageName': app.id,
                                    'sig': apk.signerHexMd5Hash,
                                    'signer': apk.signerSha256Hash,
                                    'size': apk.size,
                                    'targetSdkVersion': apk.targetSdkVersion,
                                    'uses-permission': apk.permissions.map(permission => ([permission.id, null])),
                                    'versionCode': apk.versionCode,
                                    'versionName': apk.versionName
                                }))
                            }), {})
                        })
                    }
                ]
            });
            file = await fs.readFile(filePath);
        }
        reply.send(file);
    }
));

[
    '/repo/icons/:fileData',
    '/fdroid/repo/icons/:fileData',
    '/repo/icons-320/:fileData',
    '/fdroid/repo/icons-320/:fileData',
    '/repo/:_/:__/:fileData',
    '/fdroid/repo/:_/:__/:fileData',
    '/repo/:_/:__/:___/:fileData',
    '/fdroid/repo/:_/:__/:___/:fileData',
    '/repo/:fileData.apk',
    '/fdroid/repo/:fileData.apk',
    '/repo/file/:fileData',
    '/fdroid/repo/file/:fileData'
].forEach(path => fastify.get(
    path,
    async (
        request,
        reply
    ) => {
        const isFdroidClient = request.headers['user-agent'].startsWith('F-Droid');
        if(isFdroidClient && !isProxy)
            return reply.code(400).send();
        const {
            error: {
                message: parseError
            } = {},
            value: {
                property,
                id,
                name
            }
        } = Joi.object({
            property: Joi
                .string()
                .valid(
                    'App.iconUrl',
                    'Locale.featureGraphicUrl',
                    'Screenshot.url',
                    'Apk.url'
                )
                .required(),
            id: Joi.string().required(),
            name: Joi.string()
        }).validate(JSON.parse(decodeBase64(request.params['fileData'])));
        if(parseError)
            return reply.code(400).send({ parseError });
        const
            [model, attribute] = property.split('.'),
            {
                [attribute]: url
            } = await sequelize.models[model].findOne({
                attributes: [attribute],
                where: { id }
            }) || {};
        if(!url)
            return reply.code(404).send();
        if(isFdroidClient){
            const
                {
                    status,
                    headers,
                    data
                } = await axios({
                    method: request.method,
                    url,
                    responseType: 'arraybuffer'
                });
            reply
                .code(status)
                .headers({
                    ...headers,
                    ...name ? {
                        'content-disposition': `attachment; filename=${name}`
                    } : {}
                })
                .send(data);
        }
        else
            reply.redirect(url);
    }
));

(async () => {
    await sequelize.authenticate();
    generateModels(sequelize);
    await sequelize.sync();
    await fastify.listen({
        host: fastifyHost,
        port: fastifyPort
    });
})().catch(error => fastify.log.error(error));