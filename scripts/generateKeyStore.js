import childProcess from 'node:child_process';

import {
    keytoolPath,
    keyStorePass,
    keyPass
} from '../config.js';

childProcess.spawn(
    keytoolPath,
    [
        '-genkey',
        '-keystore',
        './data/keystore',
        '-alias',
        'alias',
        '-keyalg',
        'RSA',
        '-keysize',
        '2048',
        '-validity',
        '10000',
        '-storepass:env',
        'KEY_STORE_PASS',
        '-dname',
        'CN=Unknown',
        '-keypass:env',
        'KEY_PASS'
    ],
    {
        env: {
            'KEY_STORE_PASS': keyStorePass,
            'KEY_PASS': keyPass
        }
    }
);